/*:
-------------------------------------------------------------------------------
@title Farming
@author jmoresca --> Laro Entertainment
@version 1.0
@date Aug 18, 2018
@filename Farming.js
@url http://localhost

@param Plants
@note Ignore

@param Plant Action Common Event
@text Plant Action Common Event
@type common_event
@parent Plants
@default 1

@param Plant Action Event Variable
@text Plant Action Event Variable
@type variable
@parent Plants
@default 5

@param Plantable Region Id
@text Plantable Region Id
@type number
@parent Plants
@default 1



If you enjoy my work, consider supporting me on Patreon!

* https://www.patreon.com/

If you have any questions or concerns, you can contact me at any of
the following sites:

* Main Website: none

-------------------------------------------------------------------------------
@plugindesc v1.0 - Allows you to setup farming system
@help 
-------------------------------------------------------------------------------
== Description ==

Setup plants/objects on map using Farm.json
Setup plants DB using Plants.json

Use Plant Action Common Event parameter as the Id of common event to be triggered 
when using items on plants/objects

Use Plantable Region Id for determining part of the map that can be planted

-------------------------------------------------------------------------------
 */ 
var Imported = Imported || {} ;
var Laro = Laro || {};
Imported.Laro_Farming = 1;
Laro.Farming = Laro.Farming || {};
var $gameFarm = null;

var PlantActionCommonEvent = PluginManager.parameters('Farming')['Plant Action Common Event'];
var PlantableRegionId = PluginManager.parameters('Farming')['Plantable Region Id'];
var PlantActionEventVariable = PluginManager.parameters('Farming')['Plant Action Event Variable'];

(function ($) 
{
    var _DataManager_databaseFiles = DataManager._databaseFiles;
    var _DataManager_createGameObjects = DataManager.createGameObjects;
    var _DataManager_makeSaveContents = DataManager.makeSaveContents;
    var _DataManager_extractSaveContents = DataManager.extractSaveContents;
    var _Spriteset_Map_createCharacters = Spriteset_Map.prototype.createCharacters;
    var _Game_CharacterBase_isCollidedWithCharacters = Game_CharacterBase.prototype.isCollidedWithCharacters;

    _DataManager_databaseFiles.push({ name: '$farm', src: 'Farm.json' });
    _DataManager_databaseFiles.push({ name: '$plants', src: 'Plants.json' });

    DataManager.createGameObjects = function() 
    {
        _DataManager_createGameObjects.call(this);
        $gameFarm = new Game_Farm();
    };

    DataManager.makeSaveContents = function() 
    {
        var contents = _DataManager_makeSaveContents.call(this);
        
        $gameFarm.createSaveData();

        contents.gameFarm = $gameFarm._farmData;
        return contents;
    };
    
    DataManager.extractSaveContents = function(contents) 
    {
        _DataManager_extractSaveContents.call(this, contents);

        var gameFarmData = contents.gameFarm;
        $gameFarm._farmData = gameFarmData;
        $gameFarm._setupPlants();
    };

    // Move custom gain item on seperate script
    /*
    Game_Party.prototype.gainItem = function(item, amount, includeEquip) 
    {
        var container = this.itemContainer(item);
        if (container) {
            var lastNumber = this.numItems(item);
            var newNumber = lastNumber + amount;
            container[item.id] = newNumber.clamp(0, this.maxItems(item));
            if (container[item.id] === 0) {
                delete container[item.id];
            }
            if (includeEquip && newNumber < 0) {
                this.discardMembersEquip(item, -newNumber);
            }
            $gameMap.requestRefresh();
        }
    };
    */

    Object.defineProperty(Game_Actor.prototype, 'currentItem', 
    {
        get: function() 
        {
            return this._currentItem;
        },
        set: function(currentItem) 
        {
            this._currentItem = currentItem;
        }
    });

    Game_CharacterBase.prototype.isCollidedWithCharacters = function(x, y) 
    {
        return _Game_CharacterBase_isCollidedWithCharacters.call(this, x, y) ||
            this.isCollidedWithPlants(x, y);
    };
    
    Game_CharacterBase.prototype.isCollidedWithPlants = function(x, y) 
    {
        var plant = $gameMap.plantsXyNt(x, y);

        return (plant && !(plant.plant().age < plant.plantMeta().collideAge));
    };

    Game_Map.prototype.plants = function() 
    {
        return $gameFarm.farmPlants[this._mapId] || [];
    };

    Game_Map.prototype.plantsXyNt = function(x, y)
    {
        var plant = this.plants().find(function(plant)
        {
            return plant.x === x && plant.y === y;    
        });

        return plant;
    };

    Game_Map.prototype.removePlant = function(x, y)
    {
        var gamePlant = this.plantsXyNt(x, y);
        var spriteSetMap = SceneManager._scene._spriteset;

        var spritePlant = spriteSetMap._characterSprites.find(function(characterSprite) 
        {
            return characterSprite._character == gamePlant;
        }, this);

        spriteSetMap._tilemap.removeChild(spritePlant);
        spriteSetMap._characterSprites.splice(spriteSetMap._characterSprites.indexOf(spritePlant), 1);

        $gameFarm.removePlant(this._mapId, x, y);
    };

    Game_Map.prototype.plantSeed = function(seedItemId, x, y)
    {
        var dataItem = $dataItems[seedItemId];
        var plantId = parseInt(dataItem.meta.plantId);

        var plantMeta = $plants.find(function (plant) 
        { 
            return plant.id === plantId;
        }, this);
        
        var leftMost = x - 1;
        var topMost = y - 1;
        var rightMost = x + 1;
        var bottomMost = y + 1;
        var spriteSetMap = SceneManager._scene._spriteset;

        for (var xIndex = leftMost; xIndex <= rightMost; xIndex++) 
        {
            for (var yIndex = topMost; yIndex <= bottomMost; yIndex++) 
            {
                var gamePlant = this.plantsXyNt(xIndex, yIndex);
                if(!gamePlant && $gameFarm.canPlant(xIndex, yIndex))
                {
                    var gamePlant = $gameFarm.addPlant(this._mapId, plantMeta, xIndex, yIndex, 0);

                    var spritePlant = new Sprite_Character(gamePlant);
                    spriteSetMap._characterSprites.push(spritePlant);
                    spriteSetMap._tilemap.addChildAt(spritePlant, 1);
                }
            }
        }
    };

    Spriteset_Map.prototype.createCharacters = function() 
    {
        _Spriteset_Map_createCharacters.call(this);

        $gameMap.plants().forEach(function(gamePlant) 
        {
            var spritePlant = new Sprite_Character(gamePlant);
            this._characterSprites.push(spritePlant);

            this._tilemap.addChildAt(spritePlant, 1);
        }, this);
    };

    function Game_Farm() 
    {
        this.initialize.apply(this, arguments);
    };

    Game_Farm.prototype.initialize = function() 
    {
        this._farmData = $farm;
        this._setupPlants();
    };

    Game_Farm.prototype.createSaveData = function()
    {
        for (var mapId in this.farmPlants) 
        {
            if (this.farmPlants.hasOwnProperty(mapId)) 
            {
                var plants = this.farmPlants[mapId].map(function(plant)
                {
                    return plant._plant;
                });

                this._farmData[mapId] = plants;
            }
        }
    };

    Game_Farm.prototype._setupPlants = function() 
    {
        this.farmPlants = {};
        var farmData = this._farmData;

        for (var mapId in farmData) 
        {
            if (farmData.hasOwnProperty(mapId)) 
            {
                var plants = [];

                farmData[mapId].forEach(function(plant)
                {
                    var gamePlant = new Game_Plant(mapId, plant);
                    plants.push(gamePlant);
                });

                this.farmPlants[mapId] = plants;
            }
        }
    };

    Game_Farm.prototype.removePlant = function(mapId, x, y)
    {
        this.farmPlants[mapId] = this.farmPlants[mapId].filter(function(plant)
        {
           return !(plant.x === x && plant.y === y);
        });
    };

    Game_Farm.prototype.addPlant = function(mapId, plant, x, y, age)
    {
        var farmPlant = { "x": x, "y": y, "plantId": plant.id, "name": plant.name, "age": age || 0 };
        var gamePlant = new Game_Plant(mapId, farmPlant);

        this.farmPlants[mapId] = this.farmPlants[mapId].concat([gamePlant]);

        return gamePlant;
    };

    Game_Farm.prototype.canPlant = function(x, y)
    {
        return $gameMap.regionId(x, y) == PlantableRegionId;
    };

    Game_Farm.prototype.increasePlantAge = function(n)
    {
        for (var mapId in this.farmPlants) 
        {
            if (this.farmPlants.hasOwnProperty(mapId)) 
            {
                this.farmPlants[mapId].forEach(function(plant)
                {
                    plant.increasePlantAge(n);
                });
            }
        }
    };

    Game_Farm.prototype.tryHarvest = function(x, y)
    {
        var plant = $gameMap.plantsXyNt(x, y);

        if (plant != null && plant.canHarvest())
        {
            //ItemIds refer on the quality of the harvest
            var plantMeta = plant.plantMeta();
            $gameActors.actor(1)["currentItem"] = plantMeta.itemIds[0];
            $gameMap.removePlant(x, y);

            $gameParty.gainItem($dataItems[$gameActors.actor(1)["currentItem"]], 1);
            $gameActors.actor(1)["currentItem"] = null;
        }
    };

    function Game_Plant() 
    {
        this.initialize.apply(this, arguments);
    };
    
    Game_Plant.prototype = Object.create(Game_Character.prototype);
    Game_Plant.prototype.constructor = Game_Plant;
    
    Game_Plant.prototype.initialize = function(mapId, plant)
    {
        Game_Character.prototype.initialize.call(this);
        this._mapId = mapId;
        this._plant = plant;
        Game_Plant.prototype.locate.call(this, this.plant().x, this.plant().y);
        this.refresh();
    };
    
    Game_Plant.prototype.initMembers = function() 
    {
        Game_Character.prototype.initMembers.call(this);
        this._moveType = 0;
        this._trigger = 0;
        this._starting = false;
        this._erased = false;
        this._pageIndex = -2;
        this._originalPattern = 1;
        this._originalDirection = 2;
        this._prelockDirection = 0;
        this._locked = false;
    };

    Game_Plant.prototype._plantId = function() 
    {
        return this.plant().plantId;
    };
    
    Game_Plant.prototype.plant = function() 
    {
        return this._plant;
    };

    Game_Plant.prototype.plantMeta = function() 
    {
        return $plants.find(function (plant) 
        { 
            return plant.id === this.plant().plantId;
        }, this);
    };

    Game_Plant.prototype.canHarvest = function()
    {
        var plant = this.plant();
        var plantMeta = this.plantMeta();

        return plant.age >= plantMeta.harvestAge && plant.age <= plantMeta.maxHarvestAge;
    };

    Game_Plant.prototype.canIncreaseAge = function()
    {
        var nextAge = this._plant.age + 1;
        var maxAge = this.plantMeta().growthStages.length;

        return nextAge < maxAge;
    };

    Game_Plant.prototype.increasePlantAge = function(x)
    {
        if(this.canIncreaseAge())
        {
            var plant = this._plant;
            this._plant = Object.assign({}, plant, { age: plant.age + x });
            this.refresh();
        }
    };

    Game_Plant.prototype.refresh = function() 
    {
        var plantMeta = this.plantMeta();
        var growthStage = plantMeta.growthStages[this.plant().age];

        if (growthStage.tileId > 0) 
        {
            this.setTileImage(growthStage.tileId);
            this.setDirection(1);
        } 
        else 
        {
            this.setImage(growthStage.characterName, growthStage.characterIndex);
            this.setDirection(growthStage.direction);
        }

        this.setDirectionFix(true);
        this.setPattern(1);
        this.setMoveSpeed(0);
        this.setMoveFrequency(0);
        this.setPriorityType(0);
    };

    Game_Plant.prototype.locate = function(x, y) 
    {
        Game_Character.prototype.setPosition.call(this, x, y);
        Game_Character.prototype.straighten.call(this);
        this.refreshBushDepth();
    };

    Game_Plant.prototype.refreshBushDepth = function() 
    {
        this._bushDepth = 0;
    };

    Game_Plant.prototype.isTransparent = function()
    {
        return false;
    };
})(Laro.Farming);