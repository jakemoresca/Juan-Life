/*:
-------------------------------------------------------------------------------
@title Day and Night System
@author jmoresca --> Laro Entertainment
@version 1.0
@date September 9, 2018
@filename DayNight.js
@url http://localhost

@param DayNight
@note Ignore

@param Presunrise Hour
@text Presunrise Hour
@type number
@parent DayNight
@default 5

@param Presunrise Color
@text Presunrise Color
@type text
@parent DayNight
@default -75 -75 0 50

@param Sunrise Hour
@text Sunrise Hour
@type number
@parent DayNight
@default 6

@param Sunrise Color
@text Sunrise Color
@type text
@parent DayNight
@default 0 0 0 0

@param Noon Hour
@text Noon Hour
@type number
@parent DayNight
@default 11

@param Noon Color
@text Noon Color
@type text
@parent DayNight
@default 45 45 0 -25

@param NoonEnd Hour
@text NoonEnd Hour
@type number
@parent DayNight
@default 14

@param NoonEnd Color
@text NoonEnd Color
@type text
@parent DayNight
@default 0 0 0 0

@param Presunset Hour
@text Presunset Hour
@type number
@parent DayNight
@default 18

@param Presunset Color
@text Presunset Color
@type text
@parent DayNight
@default -50 -50 0 25

@param Sunset Hour
@text Sunset Hour
@type number
@parent DayNight
@default 19

@param Sunset Color
@text Sunset Color
@type text
@parent DayNight
@default -75 -100 0 75

@param Midnight Hour
@text Midnight Hour
@type number
@parent DayNight
@default 0

@param Midnight Color
@text Midnight Color
@type text
@parent DayNight
@default -125 -125 0 125

@param DateSettings
@note Ignore

@param WeekDays
@text WeekDays
@type text
@parent DateSettings
@default Sun Mon Tue Wed Thu Fri Sat

@param Months
@text Months
@type text
@parent DateSettings
@default Cold Summer Spring Autumn

If you enjoy my work, consider supporting me on Patreon!

* https://www.patreon.com/

If you have any questions or concerns, you can contact me at any of
the following sites:

* Main Website: none

-------------------------------------------------------------------------------
@plugindesc v1.0 - Allows you to setup day and night system
@help 
-------------------------------------------------------------------------------
== Description ==
    Winter/Cold Season (tag-lamig) – November to January
    Summer/Hot Season (tag-init) – March to May
    Spring or when things grow (tag-sibol)
    Autumn or when things die off (tag-lagas)

-------------------------------------------------------------------------------
 */
var Imported = Imported || {};
var Laro = Laro || {};
Imported.Laro_DayNight = 1;
Laro.DayNight = Laro.DayNight || {};

var PresunriseHour = Number(PluginManager.parameters('DayNight')['Presunrise Hour']);
var SunriseHour = Number(PluginManager.parameters('DayNight')['Sunrise Hour']);
var NoonHour = Number(PluginManager.parameters('DayNight')['Noon Hour']);
var NoonEndHour = Number(PluginManager.parameters('DayNight')['NoonEnd Hour']);
var PresunsetHour = Number(PluginManager.parameters('DayNight')['Presunset Hour']);
var SunsetHour = Number(PluginManager.parameters('DayNight')['Sunset Hour']);
var MidnightHour = Number(PluginManager.parameters('DayNight')['Midnight Hour']);

var PresunriseColor = PluginManager.parameters('DayNight')['Presunrise Color'];
var SunriseColor = PluginManager.parameters('DayNight')['Sunrise Color'];
var NoonColor = PluginManager.parameters('DayNight')['Noon Color'];
var NoonEndColor = PluginManager.parameters('DayNight')['NoonEnd Color'];
var PresunsetColor = PluginManager.parameters('DayNight')['Presunset Color'];
var SunsetColor = PluginManager.parameters('DayNight')['Sunset Color'];
var MidnightColor = PluginManager.parameters('DayNight')['Midnight Color'];

var DateWeekDays = String(PluginManager.parameters('DayNight')['WeekDays']);
var DateMonths = String(PluginManager.parameters('DayNight')['Months']);

(function ($) 
{
    var _DataManager_createGameObjects = DataManager.createGameObjects;
    DataManager.createGameObjects = function () 
    {
        _DataManager_createGameObjects.call(this);
        $gameDayTimer = new Game_DayTimer();
    };

    var _DataManager_makeSaveContents = DataManager.makeSaveContents;
    DataManager.makeSaveContents = function () 
    {
        var contents = _DataManager_makeSaveContents.call(this);

        $gameDayTimer.createSaveData();

        contents.gameDayTimer = $gameDayTimer._timerData;
        return contents;
    };

    var _DataManager_extractSaveContents = DataManager.extractSaveContents;
    DataManager.extractSaveContents = function (contents) 
    {
        _DataManager_extractSaveContents.call(this, contents);

        var gameDayTimerData = contents.gameDayTimer;
        $gameDayTimer._timerData = gameDayTimerData;
        $gameDayTimer.setupDayTimer();
    };

    var _Scene_Map_updateMain = Scene_Map.prototype.updateMain;
    Scene_Map.prototype.updateMain = function ()
    {
        _Scene_Map_updateMain.call(this);
        var active = this.isActive();

        $gameDayTimer.update(active);
    };

    var _Game_Timer_initialize = Game_Timer.prototype.initialize;

    function Game_DayTimer() 
    {
        this.initialize.apply(this, arguments);
    };

    Game_DayTimer.prototype = Object.create(Game_Timer.prototype);
    Game_DayTimer.prototype.constructor = Game_DayTimer;

    Game_DayTimer.prototype.initialize = function ()
    {
        _Game_Timer_initialize.call(this);

        this._currentDate = 1;
        this._currentHour = 6;
        this._frames = (this._currentHour * 6) * 600;

        this._weekDays = DateWeekDays.split(" ");
        this._months = DateMonths.split(" ");

        this._currentDay = this._weekDays[0];
        this._currentMonth = this._months[0];

        this.changeScreenTintByHour();
    };

    Game_DayTimer.prototype.update = function (sceneActive)
    {
        if (sceneActive) //Todo: Add don't increment if inside house or event
        {
            this._frames++;

            if (this._currentHour < this.hour())
            {
                this._currentHour = this.hour();

                if (this._currentHour >= 24)
                {
                    this._frames = 0;

                    this.increaseDay();
                }

                this.changeScreenTintByHour();
            }
        }
    };

    Game_DayTimer.prototype.increaseDay = function (hour = 0)
    {
        this._currentHour = hour;
        this._currentDate += 1;

        updateMonth();
        updateDay();
    }

    Game_DayTimer.prototype.updateMonth = function ()
    {
        if (this._currentDate >= 31)
        {
            var index = this._months.findIndex(x => x == this._currentMonth, this);
            var nextIndex = index < this._months.length - 1 ? index + 1 : 0;

            this._currentMonth = this._months[nextIndex];
        }
    }

    Game_DayTimer.prototype.updateDay = function ()
    {
        var index = this._weekDays.findIndex(x => x == this._currentDay, this);
        var nextIndex = index < this._weekDays.length - 1 ? index + 1 : 0;

        this._currentDay = this._weekDays[nextIndex];
    }

    Game_DayTimer.prototype.createSaveData = function ()
    {
        this._timerData = {
            currentDate: this._currentDate, currentHour: this._currentHour,
            currentDay: this._currentDay, currentMonth: this._currentMonth
        };
    }

    Game_DayTimer.prototype.setupDayTimer = function ()
    {
        this._currentDate = this._timerData.currentDate;
        this._currentHour = this._timerData.currentHour;
        this._frames = (this._currentHour * 6) * 600;
        this._currentDay = this._timerData.currentDay;
        this._currentMonth = this._timerData.currentMonth;

        this.changeScreenTintByHour();
    }

    Game_DayTimer.prototype.perMinute = function ()
    {
        return Math.floor(this._frames / 60);
    };

    Game_DayTimer.prototype.perTenMinutes = function ()
    {
        return Math.floor(this.perMinute() / 10);
    };

    Game_DayTimer.prototype.hour = function ()
    {
        return Math.floor(this.perTenMinutes() / 6);
    }

    Game_DayTimer.prototype.displayTime = function ()
    {
        var currentMinute = (this.perTenMinutes() - (this.hour() * 6)) * 10;
        var formattedMinute = String(currentMinute).padEnd(2, "0");

        return `${this.hour()}:${formattedMinute}`;
    }

    Game_DayTimer.prototype.displayDay = function ()
    {
        return `${this._currentMonth} - ${this._currentDay} ${this._currentDate}`;
    }

    Game_DayTimer.prototype.changeScreenTintByHour = function ()
    {
        var screenTint;

        switch (this._currentHour)
        {
            case PresunriseHour:
                screenTint = getToneFromArgs(PresunriseColor);
                break;

            case SunriseHour:
                screenTint = getToneFromArgs(SunriseColor);
                break;

            case NoonHour:
                screenTint = getToneFromArgs(NoonColor);
                break;

            case NoonEndHour:
                screenTint = getToneFromArgs(NoonEndColor);
                break;

            case PresunsetHour:
                screenTint = getToneFromArgs(PresunsetColor);
                break;

            case SunsetHour:
                screenTint = getToneFromArgs(SunsetColor);
                break;

            case MidnightHour:
                screenTint = getToneFromArgs(MidnightColor);
                break;

            default:
                return;
        }

        $gameScreen.startTint(screenTint, 60);
    };

    function getToneFromArgs(args)
    {
        var argsArray = args.split(" ");

        var red = eval(argsArray.shift());
        var green = eval(argsArray.shift());
        var blue = eval(argsArray.shift());
        var alpha = eval(argsArray.shift());

        return [red, green, blue, alpha];
    }

    var _Scene_Map_start = Scene_Map.prototype.start;
    Scene_Map.prototype.start = function ()
    {
        _Scene_Map_start.call(this);
        this._dayTimerWindow.open();
    };

    var _Scene_Map_stop = Scene_Map.prototype.stop;
    Scene_Map.prototype.stop = function ()
    {
        _Scene_Map_stop.call(this);
        this._dayTimerWindow.close();
    };

    var _Scene_Map_terminate = Scene_Map.prototype.terminate;
    Scene_Map.prototype.terminate = function ()
    {
        this._dayTimerWindow.hide();
        _Scene_Map_terminate.call(this);
        this.removeChild(this._dayTimerWindow);
    };

    var _Scene_Map_createMapNameWindow = Scene_Map.prototype.createMapNameWindow;
    Scene_Map.prototype.createMapNameWindow = function ()
    {
        _Scene_Map_createMapNameWindow.call(this);

        this._dayTimerWindow = new Window_DayTimer();
        this.addChild(this._dayTimerWindow);
    };

    var _Scene_Map_callMenu = Scene_Map.prototype.callMenu;
    Scene_Map.prototype.callMenu = function ()
    {
        _Scene_Map_callMenu.call(this);
        this._dayTimerWindow.hide();
    };

    var _Scene_Map_launchBattle = Scene_Map.prototype.launchBattle;
    Scene_Map.prototype.launchBattle = function ()
    {
        _Scene_Map_launchBattle.call(this);
        this._dayTimerWindow.hide();
    };

    //-----------------------------------------------------------------------------
    // Window_DayTimer
    //
    // The window for displaying the time on the map screen.

    function Window_DayTimer()
    {
        this.initialize.apply(this, arguments);
    }

    Window_DayTimer.prototype = Object.create(Window_Base.prototype);
    Window_DayTimer.prototype.constructor = Window_DayTimer;

    Window_DayTimer.prototype.initialize = function ()
    {
        var wight = this.windowWidth();
        var height = this.windowHeight();
        Window_Base.prototype.initialize.call(this, 0, 0, wight, height);
        this.opacity = 0;
        this.contentsOpacity = 100;
        this._showCount = 0;
        this.refresh();
    };

    Window_DayTimer.prototype.windowWidth = function ()
    {
        return 360;
    };

    Window_DayTimer.prototype.windowHeight = function ()
    {
        return this.fittingHeight(3);
    };

    Window_DayTimer.prototype.update = function ()
    {
        Window_Base.prototype.update.call(this);
        this.refresh();

        /*
        if (this._showCount > 0 && $gameMap.isNameDisplayEnabled())
        {
            this.updateFadeIn();
            this._showCount--;
        } else
        {
            this.updateFadeOut();
        }
        */
    };

    Window_DayTimer.prototype.updateFadeIn = function ()
    {
        this.contentsOpacity += 16;
    };

    Window_DayTimer.prototype.updateFadeOut = function ()
    {
        this.contentsOpacity -= 16;
    };

    Window_DayTimer.prototype.open = function ()
    {
        //this.refresh();
        this._showCount = 150;
    };

    Window_DayTimer.prototype.close = function ()
    {
        this._showCount = 0;
    };

    Window_DayTimer.prototype.refresh = function ()
    {
        this.contents.clear();

        var width = this.contentsWidth();
        this.drawBackground(0, 0, width, this.lineHeight());
        this.drawText($gameDayTimer.displayTime(), 0, 0, width, 'center');
        this.drawText($gameDayTimer.displayDay(), 0, 32, width, 'center');
    };

    Window_DayTimer.prototype.drawBackground = function (x, y, width, height)
    {
        var color1 = this.dimColor1();
        var color2 = this.dimColor2();
        this.contents.gradientFillRect(x, y, width / 2, height, color2, color1);
        this.contents.gradientFillRect(x + width / 2, y, width / 2, height, color1, color2);
    };

    var _Laro_TC_Game_Interpreter_pluginCommand = Game_Interpreter.prototype.pluginCommand;
    Game_Interpreter.prototype.pluginCommand = function (command, args)
    {
        switch (command.toUpperCase())
        {
            case 'INCREASEDAY':
                var hour = (args != null || args[0] != null) ? args[0] : 6;
                $gameDayTimer.increaseDay(hour);
                break;
            default:
                _Laro_TC_Game_Interpreter_pluginCommand.call(this, command, args);
        }
    };
})(Laro.DayNight);