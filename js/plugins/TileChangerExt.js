//=============================================================================
// Tile Changer Extension (TileChangerExt.js)
// by jmoresca
// Last Updated: 2018.03.24 v1.10
//=============================================================================

/*:
 * @plugindesc Extended functionality of Shaz_TileChanger plugin
 * @author jmoresca
 *
 * @help
 * This plugin adds functionality to the original Shaz_TileChanger plugin.
 * Current extension: Revert Tiles
 *
 * Plugin Commands:
 * ----------------
 *
 * CopyTiles group dx dy source sx1 sy1 sx2 sy2 z-list
 * Copies tiles from another (or the same) map, where
 *   group   is the group name of the tiles
 *   dx      is the x-coordinate of the top left destination area
 *   dy      is the y-coordinate of the top left destination area
 *   source  is the 'name' of the source map in the map note
 *           or 'self' if you want to copy a section of the current map
 *   sx1     is the x-coordinate of the top left source area
 *   sy1     is the y-coordinate of the top left source area
 *   sx2     is the x-coordinate of the bottom right source area
 *   sy2     is the y-coordinate of the bottom right source area
 *   z-list  is an optional series of z values indicating which layers to copy
 *           if more than one, just use spaces between
 *           if omitted, all layers will be copied
 *
 * RevertTile mapId group
 * Reverts the group tile back to it's original appearance
 *
 * Examples:
 * ---------
 *
 * CopyTiles groupA 3 3 destroyed 5 8 9 12
 * copies the area between 5,8 and 9,12 from the 'destroyed' map to the
 * current map, with the upper left at 3,3.  All layers are copied to a group named 'groupA'
 *
 * CopyTiles farmHouse 3 3 wilted 5 8 9 12 2 3
 * copies layers 2 and 3 (the upper layers) between 5,8 and 9,12 from the
 * 'wilted' map to the current map, with the upper left at 3,3. Tiles were copied to a group named 'farmHouse'
 *
 * RevertTile 1 pigpen
 * Reverts the pigpen group tile for mapId 1
 * 
 * Change Log:
 * -----------
 * 2021.06.20  1.00  Initial version featuring RevertTile and CopyTiles with group name support.
 *
 */

var Imported = Imported || {};
var Laro = Laro || {};
Imported.Laro_ToolSystem = 1;
Laro.ToolSystem = Laro.ToolSystem || {};

(function ()
{
    var _Laro_Game_System_initialize = Game_System.prototype.initialize;
    Game_System.prototype.initialize = function ()
    {
        _Laro_Game_System_initialize.call(this);
        this._revertTiles = [];
    };

    Game_System.prototype.saveMapTile = function (group, location, tileId)
    {
        var mapId = $gameMap.mapId();
        if (!this._mapTiles[mapId])
        {
            this._mapTiles[mapId] = {};
        }
        if (!this._mapTiles[mapId][group])
        {
            this._mapTiles[mapId][group] = {};
        }

        this._mapTiles[mapId][group][location] = tileId;
    };

    Game_System.prototype.saveRevertTile = function (group, location, tileId)
    {
        var mapId = $gameMap.mapId();
        if (!this._revertTiles[mapId])
        {
            this._revertTiles[mapId] = {};
        }
        if (!this._revertTiles[mapId][group])
        {
            this._revertTiles[mapId][group] = {};
        }

        this._revertTiles[mapId][group][location] = tileId;
    };

    Game_System.prototype.restoreMapTiles = function (mapId)
    {
        var tiles = this._mapTiles[mapId] || {};

        Object.keys(tiles).forEach(group =>
        {
            Object.keys(tiles[group]).forEach(location =>
            {
                $dataMap.data[location] = tiles[group][location];
            }, this);
        }, this);

        $gameTemp.setMapDataRefresh(true);
    };

    Game_System.prototype.restoreRevertTiles = function (mapId, groupName)
    {
        var mapTiles = this._mapTiles[mapId] || {};
        var revertTiles = this._revertTiles[mapId] || {};
        
        Object.keys(revertTiles).forEach(group =>
        {
            if(group == groupName)
            {
                Object.keys(revertTiles[group]).forEach(location =>
                {
                    $dataMap.data[location] = revertTiles[group][location];
                }, this);
            }
        }, this);

        delete mapTiles[groupName];

        $gameTemp.setMapDataRefresh(true);
    };

    Game_Map.prototype.copyTiles = function (args)
    {
        var groupName = args.shift();

        var dtlx = eval(args.shift());
        var dtly = eval(args.shift());
        var src = eval(args.shift());
        var stlx = eval(args.shift());
        var stly = eval(args.shift());
        var sbrx = eval(args.shift());
        var sbry = eval(args.shift());
        var zarray = [];
        for (var i = 0; i < args.length; i++)
        {
            zarray.push(eval(args[i]));
        }
        if (zarray.length === 0)
        {
            zarray = [0, 1, 2, 3, 4, 5];
        }

        if (src.toUpperCase() === 'SELF')
        {
            var source = $dataMap;
        } else
        {
            var source = $dataMap.extraMaps[src];
        }

        var sw = sbrx - stlx + 1;
        var sh = sbry - stly + 1;

        for (var z1 = 0; z1 < zarray.length; z1++)
        {
            for (var y = 0; y < sh; y++)
            {
                for (var x = 0; x < sw; x++)
                {
                    var sx = stlx + x;
                    var sy = stly + y;
                    var dx = dtlx + x;
                    var dy = dtly + y;
                    var z = zarray[z1];
                    var dIndex = this.calcIndex($dataMap, dx, dy, z);
                    var sIndex = this.calcIndex(source, sx, sy, z);

                    var revertTile = $dataMap.data[dIndex];
                    $dataMap.data[dIndex] = source.data[sIndex];

                    $gameSystem.saveMapTile(groupName, dIndex, source.data[sIndex]);
                    $gameSystem.saveRevertTile(groupName, dIndex, revertTile);
                }
            }
        }

        $gameTemp.setMapDataRefresh(true);
    };

    var _Laro_TC_Game_Interpreter_pluginCommand = Game_Interpreter.prototype.pluginCommand;
    Game_Interpreter.prototype.pluginCommand = function (command, args)
    {
        switch (command.toUpperCase())
        {
            case 'REVERTTILES':
                $gameSystem.restoreRevertTiles(args);
                break;
            default:
                _Laro_TC_Game_Interpreter_pluginCommand.call(this, command, args);
        }
    };
})(Laro.ToolSystem);