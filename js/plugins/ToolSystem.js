/*:
-------------------------------------------------------------------------------
@title Tool System
@author jmoresca --> Laro Entertainment
@version 1.0
@date June 18, 2021
@filename ToolSystem.js
@url http://localhost

@param ToolSystem
@note Ignore


If you enjoy my work, consider supporting me on Patreon!

* https://www.patreon.com/

If you have any questions or concerns, you can contact me at any of
the following sites:

* Main Website: none

-------------------------------------------------------------------------------
@plugindesc v1.0 - Allows you to use tools, plant seeds, etc
@help 
-------------------------------------------------------------------------------
== Description ==

-------------------------------------------------------------------------------
 */

var Imported = Imported || {};
var Laro = Laro || {};
Imported.Laro_ToolSystem = 1;
Laro.ToolSystem = Laro.ToolSystem || {};

(function ($) 
{
    var _Game_Player_startMapEvent = Game_Player.prototype.startMapEvent;

    Game_Player.prototype.startMapEvent = function (x, y, triggers, normal) 
    {
        _Game_Player_startMapEvent.call(this, x, y, triggers, normal);

        var isTriggerMatched = Input.isTriggered('ok') || TouchInput.isTriggered();

        if (!$gameMap.isEventRunning() && isTriggerMatched)
        {
            var currentItem = $gameActors.actor(1)["currentItem"];

            if (currentItem == null)
            {
                $gameFarm.tryHarvest(x, y);
                return;
            }

            var dataItem = $dataItems[currentItem];

            if (dataItem.meta.type === "seed" && $gameFarm.canPlant(x, y))
            {
                $gameMap.plantSeed(currentItem, $gamePlayer.x, $gamePlayer.y);

                removeCurrentItem(dataItem, currentItem);
            }
            else if (dataItem.meta.type === "blueprint")
            {
                var direction = $gamePlayer.direction();
                var offsetNote = (dataItem.meta.offset || "0 0").split(" ");
                var offset = { x: Number(offsetNote[0]), y: Number(offsetNote[1]) };
                var height = Number(dataItem.meta.height);
                var width = Number(dataItem.meta.width);

                switch(direction)
                {
                    case 8: 
                        offset.y = offset.y - height;
                        break;
                    case 2:
                        offset.y = offset.y + 1;
                        break;
                    case 6:
                        offset.x = $gamePlayer.x + 1;
                        break;
                    case 4:
                        offset.x = $gamePlayer.x - width;
                        break;
                }

                var startingX = $gamePlayer.x + offset.x;
                var startingY = $gamePlayer.y + offset.y;

                if(!canPlaceBlueprint(startingX, startingY, direction, width, height))
                {
                    $gameMessage.add("You can't place blueprint here.");
                    return;
                }

                var groupId = generateGroupId();

                var blueprintArgs = [groupId, startingX, startingY].concat(dataItem.meta.building.split(" "));

                $gameMap.copyTiles(blueprintArgs);

                $gameMessage.add("Are you sure you want to put it here?");
                $gameMap._interpreter.setupChoices([['Yes', 'No'], 1]);
                $gameMessage.setChoiceCallback(function (responseIndex)
                {
                    if (responseIndex === 0)
                    {
                        removeCurrentItem(dataItem, currentItem);
                    }
                    else
                    {
                        var mapId = $gameMap.mapId();
                        $gameSystem.restoreRevertTiles(mapId, groupId);
                    }
                });
            }

            //ToDo: Throw/Drop item
        }
    };

    function removeCurrentItem(dataItem, currentItem)
    {
        $gameParty.gainItem(dataItem, -1);

        if (!$gameParty.hasItem(currentItem))
        {
            $gameActors.actor(1)["currentItem"] = null;
        }
    }

    function generateGroupId()
    {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c)
        {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    function canPlaceBlueprint(startX, startY, d, width, height)
    {
        var isPassable = true;

        for (var x = startX; x < startX + width; x++)
        {
            for (var y = startY; y < startY + height; y++)
            {
                isPassable = $gameMap.isPassable(x, y, d);

                if(!isPassable)
                    return isPassable;
            }
        }

        return isPassable;
    }
})(Laro.ToolSystem);